<?php
    require("animal.php");
    require("frog.php");
    require("ape.php");
    
    $sheep = new animal("shaun");
    $sungokong = new ape("kera sakti");
    $kodok = new forg("buduk");

    echo "Name: ".$sheep->name."<br>";
    echo "Legs: ".$sheep->legs."<br>";
    echo "Cold blooded: ".$sheep->cold_blooded."<br>";

    $sungokong->yell();
    $kodok->jump();
?>