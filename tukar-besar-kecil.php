<?php
function tukar_besar_kecil($string){
    //kode di sini
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $abjad2 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $angka = "0123456789-!";
    $output = "";

    for ($a = 0; $a < strlen($string); $a++)
    {
        $posisi = strpos($abjad, $string[$a]);
        $output = substr($abjad2, $posisi, 1);

        if ($posisi == false)
        {
            $posisi = strpos($abjad2, $string[$a]);
            $output = substr($abjad, $posisi, 1);

            if ($posisi == false)
            {
                $posisi = strpos($angka, $string[$a]);
                $output = substr($angka, $posisi, 1);

                if ($posisi == false)
                {
                    $output = " ";
                }
            }
        }
        
        echo $output;
    }
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo "<br><br>";
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo "<br><br>";
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo "<br><br>";
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo "<br><br>";
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>