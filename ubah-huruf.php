<?php
function ubah_huruf($string){
//kode di sini
    $abjad = "abcdefghijklmnopqrstuvwxyz";
    $output = "";

    for ($a = 0; $a < strlen($string); $a++)
    {
        $posisi = strpos($abjad, $string[$a]);
        $output = substr($abjad, $posisi + 1, 1);
        
        echo $output;
    }
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo "<br><br>";
echo ubah_huruf('developer'); // efwfmpqfs
echo "<br><br>";
echo ubah_huruf('laravel'); // mbsbwfm
echo "<br><br>";
echo ubah_huruf('keren'); // lfsfo
echo "<br><br>";
echo ubah_huruf('semangat'); // tfnbohbu

?>